import {appkey} from './config.js';
import {accessToken} from './config.js';        //api keys will not be found in git code

let thisSessionLink = "Link not yet determined";    //variable storing the webrtc:// ... link to view the live stream video
let thisSessionId = "ID not yet determined";        //variable storing the SessionID of this video

/* (String) getRequestEncoding():
* -Purpose: helper function which creates an encoded URL using the given appkey, accesstoken, and current timestamp.
*           uses HmacSHA256 and hashBase64 encoding from imported CryptoJS file
* -Return : string containing the encodedURL to be concatenated with the commandURL when making API request
* -When?  : called everytime an API request is made
*
* -Details and instructions can be found on:  https://cloud.tencent.com/document/product/1240/90943
*/
function getRequestEncoding(){

    // 1. Get date and time
    let now = new Date();
    let timestamp = (now.getTime()/1000).toFixed();

    // 2. Combine content of appkey and timestamp
    let signingContent = "appkey="+ appkey + "&timestamp=" + timestamp;

    // 3. Calculate HMAC-SHA256 using content and accessToken
    let hash = CryptoJS.HmacSHA256(signingContent, accessToken);

    // 4. Base64 Hash encoding
    let hashInBase64 = hash.toString(CryptoJS.enc.Base64);

    // 5. URL encode
    signature = encodeURIComponent(hashInBase64)   

    // 6. Return concatenated values as a string
    return 'timestamp=' + timestamp + "&signature=" + signature + "&appkey=" + appkey;
}

/* (void) openSession():
* -Purpose: the first step in creating a live session, opening the session room and updating the sessionLink/sessionID
* -Results: updates variables 'thisSessionLink' and 'thisSessionId'
* 
* -Details and instructions can be found on: https://cloud.tencent.com/document/product/1240/90944
*/
function openSession(){

    //-----------Given API code from PostMan-----------//
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
        "Header": {},
        "Payload": {
            "UserId": "mydemo_test",
            "DriverType": 1,
            "Protocol": "webrtc"
        }
    });

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };
    //^^---------Given API code from PostMan---------^^//


    //fetch using the commandURL+encodedURL with the given 'requestOptions'
    fetch("https://gw.tvs.qq.com/v2/ivh/streammanager/streamservice/newsession?"  + getRequestEncoding(), requestOptions)
      .then(response => response.json())
      .then(response =>{

          //Update thisSessionId and thisSessionLink via accessing JSON parameters
          thisSessionId = response["Payload"].SessionId;
          thisSessionLink = response["Payload"].PlayStreamAddr;

          //call the startSession function on THIS function
          startSession(thisSessionId, thisSessionLink);
          
      })
      .catch(error => console.log('error', error));
}

/* (void) startSession((string) givenSessionId, (string) givenSessionLink):
* -Purpose: using the given ID and Link, start the session and then call openVideo on the Link to display video
* -Results: starts the session and calls 'openVideo' so we can see the video
* 
* -Details and instructions can be found on: https://cloud.tencent.com/document/product/1240/90945
*/
function startSession(givenSessionId, givenSessionLink){

  //-----------Given API code from PostMan-----------//
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
      "Header": {},
      "Payload": {
          "SessionId": givenSessionId
      }
  });

  var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
  };
  //^^---------Given API code from PostMan---------^^//

  //fetch using the commandURL+encodedURL with the given 'requestOptions'
  fetch("https://gw.tvs.qq.com/v2/ivh/streammanager/streamservice/startsession?" + getRequestEncoding(), requestOptions)
    .then(response => response.text())
    .catch(error => console.log('error', error));

  //after opening and starting the session, call the openVideo() helper function to display video 
  openVideo(givenSessionLink);
  
}

/* (void) sendText((string) givenText, (string) givenSessionId):
* -Purpose: sends a command to AI-bot to begin speaking
*     -string givenText: a string of legible text in chinese or english for bot to say 
*     -string givenSessionId: the session to send this text to
* 
* -Details and instructions can be found on: https://cloud.tencent.com/document/product/1240/90946
*/
function sendText(givenText, givenSessionId){

    //-----------Given API code from PostMan-----------//
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
        "Header": {},
        "Payload": {
            "SessionId": givenSessionId,
            "Command": "SEND_TEXT",
            "Data": {
                "Text": givenText,
                "Interrupt": false
            }
        }
    });
    
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };
    //^^---------Given API code from PostMan---------^^//
    
    //fetch using the commandURL+encodedURL with the given 'requestOptions'
    fetch("https://gw.tvs.qq.com/v2/ivh/streammanager/streamservice/command?"  + getRequestEncoding(), requestOptions)
      .then(response => response.text())
      .catch(error => console.log('error', error));
}

/* (void) closeSession():
* -Purpose: using the saved 'thisSessionId', close the session and video
* 
* -Details and instructions can be found on: https://cloud.tencent.com/document/product/1240/90949
*/
function closeSession(){

  //-----------Given API code from PostMan-----------//
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
      "Header": {},
      "Payload": {
          "SessionId": thisSessionId
      }
  });

  var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
  };
  //^^---------Given API code from PostMan---------^^//
  
  //fetch using the commandURL+encodedURL with the given 'requestOptions'
  fetch("https://gw.tvs.qq.com/v2/ivh/streammanager/streamservice/closesession?" + getRequestEncoding(), requestOptions)
    .then(response => response.text())
    .then(result => console.log("closing session: " + result))
    .catch(error => console.log('error', error));
}

/* (void) sendTextPromtpt():
*         -sends a prompt to the console asking for user input for AI bot
*/
function sendTextPrompt(){
  var textEntered = prompt("Enter something for the AI to say:");
  sendText(textEntered, thisSessionId);
}

/* (void) openVideo((string) givenLink):
* -Purpose: function given by Tencent TCPlayer to create a video player as an html element 
*     -string givenLink: webrtc://...    link provided to live stream 

* -Details and instructions can be found on: https://cloud.tencent.com/document/product/881/77877
*/
function openVideo(givenLink){
    let player = TCPlayer ('player-container-id', { });
    player.src(givenLink);   
}
