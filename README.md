# Goclouds Tencent Bot
## Project Overview
Used 'Tencent Cloud AI Digital Human' API to connect with a ChatGPT text output as well as
our goClouds database. The goal is for ChatGPT to generate text using its database as well as
our goClouds databse, explaining some conclusions and trends founds in the data. Meanwhile, 
this project generates a live interactive Digital-Human video stream for the user.  

## Project Achievement
Successfully implemented API related procedures of generating URL encoding,
function implementations using JavaScript fetch, as well as basic user testing via HTML LiveServer.
Acheived a state where user could enter any given text, and almost immediately watch as a AI Digital Human
animates and speaks that given line back.
## Project Takeaways
Successfully learned and implemented front end development skills : HTML, JavaScript
Employed basic API usage concepts of appkey/token practice, testing via Postman.
Employed external tools such as using Tecent's WebPlayer to embed a liveStream onto html.

## Project Conclusion
Ultimately, we decided to go a different path upon reaching this checkpoint in the project.
The livestream was very dependent on the end-user's wifi speed, and could lead to occasional
lagging which causes certain portions of livestream to be skipped. We wanted to move towards
a different but similar product that generates short mp4 files instead of a livestream link.
